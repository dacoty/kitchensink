import yaml
import re
import getpass


# password validation script in python
# made to be flexible( or i over-engineered it)
# since making a complicated regex with hard rules is a pain to maintain,
# this script demonstrates a more flexible way to check if a password is valid

# we create a rules.yaml file, populate it, and then use this script to load the file and then check the password given
# against the rules.

# func to load the rules file
def load_rules_file():
    with open("rules.yaml", 'r') as rules:
        try:
            loaded_data = yaml.safe_load(rules)
            return loaded_data
        except yaml.YAMLError as error:
            return error


# func to check the password
def check_password(password):
    try:
        # load the rules up
        loaded_data = load_rules_file()
        # set the rules here
        min_characters = loaded_data['rules']['minchars']  # 6
        max_characters = loaded_data['rules']['maxchars']  # 20
        uppercase_required = loaded_data['rules']['uppercase']  # true
        lowercase_required = loaded_data['rules']['lowercase']  # true
        numbers_required = loaded_data['rules']['numbers']  # true
        symbols_required = loaded_data['rules']['symbols']  # true
        password_length = len(password)
        # check password length
        if password_length >= min_characters and password_length <= max_characters:
            print("Password length ---- ✓")
        else:
            print(
                f"FAIL -  your password is {password_length} characters long, it needs to be {min_characters} characters to {max_characters} characters! ")

        if uppercase_required:
            check_rule_uppercase(password)

        if lowercase_required:
            check_rule_lowercase(password)

        if numbers_required:
            check_rule_numbers(password)

        if symbols_required:
            check_rule_symbols(password)

    except Exception as error:
        print(error)


def check_rule_uppercase(p):
    # find uppercase letters
    has_uppercase_letters = re.search('[A-Z]', password)
    if has_uppercase_letters:
        print("uppercase letters ---- ✓")
    else:
        print("FAIL - you need at least 1 uppercase letter in your password")


def check_rule_lowercase(p):
    has_lowercase_letters = re.search('[a-z]', p)
    if has_lowercase_letters:
        print("lowercase letters ---- ✓")
    else:
        print("FAIL - you need at least 1 lowercase letter in your password")


def check_rule_numbers(p):
    has_numbers = re.search('\d', p)
    if has_numbers:
        print("numbers ---- ✓")
    else:
        print("FAIL - you need at least 1 number in your password")


def check_rule_symbols(p):
    has_symbols = re.search('[!@#$%^&*]', p)
    if has_symbols:
        print("symbols ---- ✓")
    else:
        print("FAIL - you need at least 1 symbol in your password")


if __name__ == '__main__':
    password = getpass.getpass(
        prompt="Password validator - please enter your password( will not be shown here, but will be shown after the check): ")
    check_password(password)
    print(f"your password in clear text is {password}")
